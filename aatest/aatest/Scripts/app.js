﻿angular.module('flights', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/',
            {
                templateUrl: '/Home/Home',
                controller: 'selectController'
            })
            .when('/list',
            {
                templateUrl: '/Home/List',
                controller: 'listController'
            });
    })
    .factory('dataFactory', ['$http', function ($http) {
        var urlBase = '/home';
        var dataFactory = {};

        dataFactory.getFlights = function (from, to) {
            return $http.get(urlBase + "/getFlights/?from=" + from + "&to=" + to);
        }

        dataFactory.getAirlines = function () {
            return $http.get(urlBase + "/getAirlines");
        }

        return dataFactory;
    }])
    .directive('sortButton',function() {
        return {
            restrict: 'AE',
            templateUrl: '/Home/SortButton',
            scope: {
                sortField: '=sortField',
                fieldName: '=fieldName',
                sortType: '=sortType',
                sortReverse:'=sortReverse'
    }
        };
    })
    .controller('selectController', ['$scope', 'dataFactory', function ($scope, dataFactory) {
        dataFactory.getAirlines().then(function (rsp) {
            $scope.airlines = rsp.data;
        }, function (err) {

        });
        $scope.filterToCodes = function (obj) {
            return obj.code !== $scope.fromAirline.code;
        };
        $scope.filterFromCodes = function (obj) {
            return obj.code !== $scope.toAirline.code;
        };
        function watchAirline(x) {
            if ($scope.fromAirline.code === undefined ||
                $scope.toAirline.code === undefined ||
                $scope.fromAirline.code === '' ||
                $scope.toAirline.code === '') $scope.link = '';
            else {
                $scope.link = "#!/list?from=" + $scope.fromAirline.code + "&to=" + $scope.toAirline.code;
            }
        }
        $scope.$watch('fromAirline.code', watchAirline);
        $scope.$watch('toAirline.code', watchAirline);
    }])
    .controller('listController', ['$scope', '$routeParams', 'dataFactory', function ($scope, $routeParams, dataFactory) {
        dataFactory.getFlights($routeParams.from, $routeParams.to).then(function (rsp) {
            $scope.flights = rsp.data;
        }, function (err) {

        });

        $scope.sortType = 'firstClassPrice';
        $scope.sortReverse = false;
    }]);