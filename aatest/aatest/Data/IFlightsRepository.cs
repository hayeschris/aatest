﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using aatest.Models;
using CsvHelper;

namespace aatest.Data
{
    public interface IFlightsRepository
    {
        IEnumerable<Flight> GetFlights(string from, string to);
        IEnumerable<Airline> GetAirlines();
    }

    public class FlightsRepository : IFlightsRepository
    {
        public IEnumerable<Flight> GetFlights(string @from, string to)
        {
            using (var rdr = new StreamReader(HttpContext.Current.Server.MapPath("~/Data/flights.csv")))
            {
                return new CsvReader(rdr).GetRecords<Flight>()
                    .Where(a => a.From == from && a.To == to)
                    .ToArray();
            }
        }

        public IEnumerable<Airline> GetAirlines()
        {
            using (var rdr = new StreamReader(HttpContext.Current.Server.MapPath("~/Data/airports.csv")))
            {
                return new CsvReader(rdr).GetRecords<Airline>()
                    .ToArray();
            }
        }
    }
}