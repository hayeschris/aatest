﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using aatest.Data;
using aatest.Models;
using CsvHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace aatest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFlightsRepository _flightsRepository;

        public HomeController(IFlightsRepository flightsRepository)
        {
            _flightsRepository = flightsRepository;
        }
        // GET: Flights
        public ActionResult Index() => View();

        [HttpGet]
        public ActionResult Home(string id) => PartialView("Home");

        public ActionResult List() => PartialView("List");

        public ActionResult SortButton() => PartialView("SortButton");

        public ActionResult GetFlights(string from, string to) => GetJsonResult(_flightsRepository.GetFlights(from, to));

        private static ActionResult GetJsonResult<T>(IEnumerable<T> model)
        {
            return new ContentResult
            {
                ContentType = "application/json",
                Content = JsonConvert.SerializeObject(model,
                    new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }),
                ContentEncoding = Encoding.UTF8
            };
        }

        public ActionResult GetAirlines() => GetJsonResult(_flightsRepository.GetAirlines());
    }
}