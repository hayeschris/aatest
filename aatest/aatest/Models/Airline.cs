﻿using System;

namespace aatest.Models
{
    public class Airline
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class Flight
    {
        public string From { get; set; }
        public string To { get; set; }
        public string FlightNumber { get; set; }
        public string Departs { get; set; }
        public string Departs24 => DateTime.Parse(Departs).ToString("HHmm");
        public string Arrives { get; set; }
        public double MainCabinPrice { get; set; }
        public double FirstClassPrice { get; set; }
    }
}